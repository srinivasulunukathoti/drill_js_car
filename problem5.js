// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained 
//from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function findBefore2000YearCars(data) {
    //using filter method filtering the which you want.
    let years = data.filter((car )=> {
        return car.car_year <2000;
    });
    return years || [];
}
//exports the code
module.exports = {findBefore2000YearCars};