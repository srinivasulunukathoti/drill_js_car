// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 
// by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:

function findTheCarById(data,id) {
    // using find method getting particular id details
    return data.find(car => car.id ===id) || null;
}
//exports the code
module.exports = {findTheCarById};