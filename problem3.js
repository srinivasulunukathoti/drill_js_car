//The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car 
// model names into alphabetical order and log the results in the console as it was returned.

function arrangeAllCarAlphabeticalOrder(data) {
    return data.reduce((accumulator, current) => {
        if (accumulator.length === 0) {
            // If accumulator is empty, add the first element
            accumulator.push(current);
        } else {
            let inserted = false;
            for (let index = 0; index <accumulator.length; index++) {
                if (current.car_make < accumulator[index].car_make) {
                    accumulator.splice(index, 0, current);
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                accumulator.push(current);
            }
        }
        return accumulator;
    }, []);
}
//exports the code
module.exports = {arrangeAllCarAlphabeticalOrder};