// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and 
// model of the last car in the inventory is?  Log the make and model into the console in the format of

function findLastCar(data) {
    return data[data.length-1];
}
// exports the code.
module.exports = {findLastCar};