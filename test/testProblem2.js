const findLastCar = require('../problem2');
const data = require('../data');

try {
    const lastCar = findLastCar.findLastCar(data.data);
    console.log(lastCar);
} catch (error) {
    console.log("Insufficiant data");
}