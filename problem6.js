// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array 
//that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function findOnlyBMWAndAudi(data) {
    //using filter method filtering the which you want.
    let cars = data.filter((car) => {
        // check data is true or false.
        if(car.car_make === "BMW" || car.car_make === "Audi")
        {
            return car.car_make;
        }
    });
    return cars || null;
}
//exports the code
module.exports = {findOnlyBMWAndAudi};
